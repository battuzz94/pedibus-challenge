---
author:
- Andreoli Maddalena | Battistello Andrea | Di Bello Antonio
date: February 2017
---

A solution to the Walking bus problem
===

The walking bus problem
===

Given a complete weighted directed graph over a set of N points in a 2D
grid, we want to find a spanning tree, rooted in point 0 such that it
complies with the following constraint:

-   every path from the root to a node X must not be longer than
    alpha times the shortest path from the root to X, with alpha
    as a fixed parameter.

Every tree that follows the above constraint is a feasible solution.
Among all feasible solutions, we want to find the one that has:

1.  minimum number of leaves (primary objective function)

2.  minimum overall weight, that is the sum of the weight of all the
    arcs of the solution (secondary objective function)

The goal is to produce a solver that is able to find a good approximate
(or even optimal) solution for the problem above, within a maximum
computation time of 1 hour.

![Solution example for 10 nodes. Note that some arcs are missing only
because they won't comply with the path length
constraint](https://bytebucket.org/battuzz94/pedibus-challenge/raw/9102244196ba302f47fbb3423a6fd900e270ac40/walking_bus_example.png)

Greedy procedure
===

The optimal solution for this problem is hard to find by extensively
enumerating all feasible spanning trees, so we opted for a greedy
procedure that let us find a rather good starting solution. This
procedure will then be used by the solver to complete the solution
(since the solver will only enumerate all partial solution within a
certain depth).

Preprocessing
-------------

After reading the input, all the nodes are sorted by their distance from
the root (node 0), and are numbered consequently (e.g. node 1 will be
the closest node from the root, node 2 the second closest and so on).
Every following phase will consider the nodes in this order.

More constrained leaf first, closest node first
-----------------------------------------------

After this preprocessing step, the procedure will build the solution
incrementally and operates in the following way:

1.  find the leaf that can be connected with the lowest (strictly
    positive) number of nodes

2.  expand the frontier of the solution by adding the closest node to
    such leaf

3.  if there are no leaves or all the leaves can't connect to any other
    node, connect the closest node to the root.

Some more optimizations are done to improve the performance of this
greedy procedure:

-   After adding a node to a leaf, check the path from the leaf to the
    root. If there is a pair of adjacent nodes U and V such that by
    swapping their position the path length to the leaf is reduced, then
    U and V will be swapped. We can see that if the path length to the
    leaf is reduced then surely more nodes can be connected, thus this
    step will always improve the solution.

-   After connecting all nodes, an *eliminate path* procedure is
    executed for every leaf.

-   After connecting all nodes, a *hill climbing iterated* procedure is
    executed

Eliminate path
--------------

This procedure tries to reduce the number of leaves. When called from a
node, this procedure will try to distribute all the nodes along the path
to the root across all other existing paths. This procedure acts in the
following way:

1.  sort the nodes to be eliminated in increasing order of distance to
    the root

2.  for every node u in such an order, find the closest path
    x -> y (point-segment distance) and include node u such
    that the new path is: x -> u -> y.

Hill climbing iterated
----------------------

This procedure will alternatively execute two hill climbing procedures
until convergence.

-   The first hill climbing procedure applies actions of *type 1*, that
    are formulated in this way: a1 = <u, v> --> connect
    node u to node v that is, node v will be the father of node
    u.

-   The second hill climbing procedure applies actions of *type 2*, that
    are formulated in this way: a2 = <u, v, w> -->
    include node u in the path v -> w that is, the new path
    will be v -> u -> w.

Our solver
==========

Our solver operates in 3 phases after a lightweight preprocessing. Each
phase is given an equal amount of elaboration time (1/3 of the maximum
available time at our disposal). Every phase has also a timeout of 5
minutes, so if no improvement has been found in 5 minutes the solver
will move to the next phase.

Presolve phase
--------------

In this first phase, the main goal is to **minimize the number of
leaves** (primary objective function). Starting from an empty solution,
we add every node until we reach a certain depth. The algorithm used for
this phase is an **iterative deepening** starting from a fixed depth of
11 nodes and at each step we consider **only 2 nodes** : the **closest
leaf** in the current partial solution and the **root**. In order to
improve execution speed, we impose a **strict bound** that will cut out
all the partial solutions that have a number of leaves equal or higher
to the number of leaves of the best solution found. Note that this bound
is strict as it cuts out also some partial solutions that may have the
same number of leaves but lower overall weight. Since the goal is to
minimize the number of leaves, we are not concerned about the weight of
the solution in this phase. When the maximum depth is reached, the
solution is completed via a greedy procedure (described above). This
procedure works very well to find a good lower bound for the following
extensive search.

Solve phase
-----------

In this second phase, the solver will try to do a second iterative
deepening like the presolve phase, but also **increases the branching
factor** at every step. This allows the solver to extend the search. The
initial maximum depth is again 11 nodes. Note that also the solve phase
the bound is very strict (like the presolve phase) because we are more
concerned about minimizing the leaves and a shallower bound takes much
more elaboration time.

Perturbation phase
------------------

This last phase will begin a series of random perturbations on the best
solution found so far. Each perturbation consist of two steps:

1.  perform a certain number of actions of *type 2*

2.  execute a *hill climbing iterated* on the modified solution

This approach is inspired from the **simulated annealing** procedure, as
it performs initially a random walk (high number of random moves) and
slows down over time (lower number of random moves). Unlike simulated
annealing, though, this approach **forces an improvement** and always
**restart from the best solution** found.

Executing the solver
====================

Execute the solver with this command:

    java -jar Pedibus.jar [options] [<input.dat> [<output.sol>]]

The available options for the solver are:

-   set the maximum elaboration time that should be used by the solver.
    Default value is 45 minutes. Default unit is second, but also 5m or
    1h are available.

-   open a new window that shows the current best solution. Within the
    frame some actions can be done:

    -   by moving the mouse cursor on a node, all the feasible moves
        will be shown in red

    -   it is possible to drag with the mouse from one node to another
        and add that edge in the solution. Note that all the updates
        done inside the GUI won't be part of the generated solution.
