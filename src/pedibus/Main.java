package pedibus;

import pedibus.gui.GraphViewer;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by andrea on 23/01/17.
 */
public class Main {
    private static boolean startGUI = false;
    private static GraphViewer viewer = null;
    private static long maxElaborationTime = 45*60;      // Set to 45 minutes
    private static long initTime = 0;
    private static final float PRESOLVE_TIME_FRACTION = 1/3.0f;
    private static final float SOLVE_TIME_FRACTION = 1/3.0f;
    private static final long TIMEOUT = 5*60;            // Set a timeout of 5 minutes
    private static long presolveElaborationTime;
    private static long solveElaborationTime;
    private static int initialIterations = 11;
    private static int branchFactor = 4;

    public static void main(String[] args) {
        List<String> remainingArgs = parseArgs(args);
        Graph g = readInput(remainingArgs);


        presolveElaborationTime = (long) (maxElaborationTime * PRESOLVE_TIME_FRACTION);
        solveElaborationTime = (long) (maxElaborationTime * SOLVE_TIME_FRACTION);
        initTime = System.currentTimeMillis();
        if (g.getN() <= 30)
            initialIterations = g.getN();
        if (g.getN() >= 250)
            branchFactor = 3;

//        initialIterations += g.getSureLeafCount();

        Solver solver = new Solver();

        if (startGUI) {
            viewer = new GraphViewer(g);
            viewer.setVisible(true);
            solver.setViewer(viewer);
        }

        // Presolve phase
        System.out.println("Starting the presolve phase");
        solver.setSolverState(Solver.PRESOLVE);
        solver.setTimeout(TIMEOUT);
        for (int i = initialIterations; i <= g.getN(); i++) {
            solver.setBranchFactor(1);
            System.out.println("Iteration: " + (i - initialIterations + 1));
            solver.partialSolve(g, i);
            writeOutput(remainingArgs, solver.getBestSol());
            if (presolveTimeFinished() || solver.timeoutExpired())
                break;
        }

        // Solve phase
        System.out.println("Starting the solve phase");
        solver.setSolverState(Solver.SOLVE);
        solver.setTimeout(TIMEOUT);
        for (int i = 0;i+initialIterations <= g.getN(); i++) {
            System.out.println("Iteration: " + (i+1));
            solver.setBranchFactor(branchFactor+i);
            solver.partialSolve(g, initialIterations+i);
            writeOutput(remainingArgs, solver.getBestSol());
            if (solveTimeFinished() || solver.timeoutExpired())
                break;
        }

        // Perturbation phase
        System.out.println("Starting perturbation phase");
        Graph solution = solver.getBestSol();
        long startingPerturbationTime = System.currentTimeMillis();
        for (int i = 0; i < 2000; i++) {
            if (solution.simulatedAnnealing()) {
                startingPerturbationTime = System.currentTimeMillis();
                System.out.println("Update: #leaves: " + solution.getSolutionLeaves() + " risk: " + solution.getSolutionRisk());
            }

            if (executionTimeFinished() || ((System.currentTimeMillis() - startingPerturbationTime)/1000 > TIMEOUT))
                break;
        }

        if (startGUI) {
            viewer.setGraph(solver.getBestSol());
        }
        writeOutput(remainingArgs, solver.getBestSol());
        printOutput(solver.getBestSol());
    }

    private static void printOutput(Graph solution) {
//        for (int i = 1; i <= solution.getN(); i++)
//            System.out.println(solution.points.get(i).idx() + " " + solution.points.get(solution.getSolution()[i]).idx());
        System.out.println("# of leaves: " + solution.getSolutionLeaves());
        System.out.println("Total risk: " + solution.getSolutionRisk());
    }

    private static void printUsage() {
        System.out.println("Usage: java -jar pedibus.jar [<input_file.dat> [<output_file.sol>]]");
        System.out.println("If input file is not specified, then the program will read from standard input");
        System.out.println("If output file is not specified, then the program will create the file <input_file>.sol\n");
        System.out.println("Options:");
        System.out.println("-t timeLimit: specifies the maximum amount of time allowed for computation. Default time unit is second, but also '1h' and '6m' are accepted");
        System.out.println("--gui : starts a JFrame to display the solution and lets you the possibility of changing the solution via drag and drop");
        System.out.println("--help : displays this message");
    }

    private static Graph readInput(List<String> args) {
        MyInputReader reader = null;

        if (args.size()>0){
            try {
                reader = new MyInputReader(args.get(0));

            } catch (FileNotFoundException e) {
                System.out.println(args.get(0) + ": input file not found");
                printUsage();
                System.exit(0);
            }
        }
        else {
            reader = new MyInputReader();
        }

        reader.readInput();

        Graph g = new Graph(reader.getN(), reader.getAlpha(), reader.getCoordX(), reader.getCoordY(), reader.getRisk());
        return g;
    }

    private static void writeOutput(List<String> args, Graph g) {
        List<Coord> points = g.getPoints();
        String fileName = "out.sol";

        if (args.size() > 1) {
            fileName = args.get(1);
        }
        else if (args.size() > 0) {
            File f = new File(args.get(0));
            fileName = f.getName();
            int pos = fileName.lastIndexOf('.');
            if (pos > 0) {
                fileName = fileName.substring(0, pos);
            }
            fileName += ".sol";
        }

        try (FileWriter writer = new FileWriter(fileName)) {
            for (int i = 1; i <= g.getN(); i++)
                writer.write(points.get(i).idx() + " " + points.get(g.getSolution()[i]).idx() + "\n");
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static List<String> parseArgs(String[] args) {
        List<String> remainingArgs = new ArrayList<>();
        for (int i = 0; i < args.length; i++) {
            if (args[i].equalsIgnoreCase("--gui") || args[i].equalsIgnoreCase("-gui")) {
                startGUI = true;
            }
            else if (args[i].equalsIgnoreCase("--help") || args[i].equalsIgnoreCase("help")) {
                printUsage();
                System.exit(0);
            }
            else if (args[i].equals("-t") || args[i].equals("--t")) {
                try {
                    String time = args[i+1];
                    i++;
                    int t = Integer.parseInt(time.replaceAll("[^0-9]", ""));
                    if (time.endsWith("m")) {
                        t *= 60;
                    }
                    else if (time.endsWith("h")) {
                        t *= (60*60);
                    }
                    maxElaborationTime = t;
                }
                catch (Exception e) {
                    printUsage();
                    System.exit(0);
                }
            }
            else {
                remainingArgs.add(args[i]);
            }
        }
        return remainingArgs;
    }

    public static boolean executionTimeFinished() {
        return ((System.currentTimeMillis() - initTime) / 1000) >= maxElaborationTime;
    }

    public static boolean presolveTimeFinished() {
        return ((System.currentTimeMillis() - initTime) / 1000) >= presolveElaborationTime;
    }

    public static boolean solveTimeFinished() {
        return ((System.currentTimeMillis() - initTime) / 1000) >= solveElaborationTime;
    }
}




