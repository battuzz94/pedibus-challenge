package pedibus;

/**
 * Created by andrea on 25/01/17.
 */
public class Coord {
    double x, y;
    int index;
    public Coord(double x, double y, int index) {
        this.x = x;
        this.y = y;
        this.index = index;
    }
    public double X() {
        return x;
    }
    public double Y() {
        return y;
    }
    public int idx() { return index; }
    public void setX(double x) {this.x = x;}
    public void setY(double y) {this.y = y;}
    public double distTo(Coord c2) {
        return Math.sqrt(Math.pow(c2.X() - x, 2.0) + Math.pow(c2.Y() - y, 2.0));
    }
}
