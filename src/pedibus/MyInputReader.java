package pedibus;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by andrea on 25/01/17.
 */
class MyInputReader {
    int N = 0;
    double alpha = 0.0;
    int[] coordX = null, coordY = null;
    double[][] risk = null;

    String filename;
    Scanner in = null;

    public MyInputReader(String filename) throws FileNotFoundException {
		in = new Scanner( new FileInputStream(filename));
		this.filename = filename;
    }
    
    public MyInputReader(){
    	in = new Scanner(System.in);
    }

    public int getN() {
        return N;
    }
    public double getAlpha() {
        return alpha;
    }
    public int[] getCoordX() {
        return coordX;
    }
    public int[] getCoordY() {
        return coordY;
    }
    public double[][] getRisk() {
        return risk;
    }

    void readInput() {
        boolean readInput = true;
        
        while (readInput) {
            String line = in.nextLine();

            String[] tokens = line.split(" ");


            if (tokens[0].compareTo("param") == 0) {
                tokens[1] = tokens[1].split("\\[")[0];
                switch(tokens[1]) {
                    case "n" :
                        N = Integer.valueOf(tokens[tokens.length - 1]);
                        coordX = new int[N + 1];
                        coordY = new int[N + 1];
                        risk = new double[N + 1][N + 1];
                        break;

                    case "alpha" :
                        alpha = Double.valueOf(tokens[tokens.length - 1]);
                        break;

                    case "coordX" :
                        String lx = in.nextLine();
                        while (lx.compareTo(";") != 0) {
                            String[] tok = lx.trim().split("\\s+", -1);
                            for (int t = 0; t < tok.length; t+=2) {
                                int idx = Integer.valueOf(tok[t]);
                                int val = Integer.valueOf(tok[t+1]);
                                coordX[idx] = val;
                            }
                            lx = in.nextLine();
                        }
                        break;
                    case "coordY" :
                        String ly = in.nextLine();
                        while (ly.compareTo(";") != 0) {
                            String[] tok = ly.trim().split("\\s+", -1);
                            for (int t = 0; t < tok.length; t += 2) {
                                int idx = Integer.valueOf(tok[t]);
                                int val = Integer.valueOf(tok[t+1]);
                                coordY[idx] = val;
                            }
                            ly = in.nextLine();
                        }
                        break;
                    case "d":
                        in.nextLine();

                        for (int i = 0; i <= N; i++) {
                            String l = in.nextLine();
                            String[] tok = l.split("[ \t]");
                            for (int j = 1; j < tok.length; j++) {
                                risk[i][j-1] = Double.valueOf(tok[j]);
                            }
                        }
                        readInput = false;
                        break;
                }
            }
        }
    }
}
