package pedibus;

/**
 * Created by andrea on 25/01/17.
 */

// Action is a tuple node -> parent node
public class Action {
    private int node;
    private int parent;
    private double value;

    public Action(int node, int parent) {
        this.node = node;
        this.parent = parent;
    }
    public void setValue(double value) {
        this.value = value;
    }

    public double getValue() {
        return this.value;
    }
    public int getNode() {
        return this.node;
    }
    public int getParent() {
        return this.parent;
    }

}
