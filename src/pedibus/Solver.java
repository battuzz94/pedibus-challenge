package pedibus;

import pedibus.gui.GraphViewer;

import java.util.*;

/**
 * Created by andrea on 27/01/17.
 */
public class Solver {
    public static final int SOLVE = 1;
    public static final int PRESOLVE = 0;
    private Graph bestSol = null;
    private int nleaves = Integer.MAX_VALUE;
    private double risk = Double.MAX_VALUE;
    private int BRANCH_FACTOR = 4;
    private boolean decreaseBranchFactor = false;
    private GraphViewer viewer = null;


    private int solverState = PRESOLVE;
    private long timeout = Long.MAX_VALUE;
    private long lastUpdateTime = System.currentTimeMillis();

    public Solver() {}

    public void setDecreaseBranchFactorByDistance(boolean flag) {
        this.decreaseBranchFactor = flag;
    }

    public void stupidSolve(Graph g) {
        stupidSolve(g, 1);
    }

    private void stupidSolve(Graph g, int n) {
        if (n > g.getN()) {
            if (g.getSolutionLeaves() < nleaves || g.getSolutionLeaves() == nleaves && g.getSolutionRisk() < risk) {
                bestSol = g.softCopy();
                nleaves = g.getSolutionLeaves();
                risk = g.getSolutionRisk();
            }
            return;
        }
        ArrayList<Action> actions = myGetActions(g, n);

        for (int i = 0; i < actions.size(); i++) {
            Graph g2 = g.softCopy();
            g2.updateSolution(actions.get(i));
            stupidSolve(g2, n + 1);
        }
    }

    private ArrayList<Action> myGetActions(Graph g, int n) {
        int branchFactor = getBranchFactor(g, n);
        ArrayList<Action> actions = new ArrayList<>();
        for (int j = g.getN(); j >= 0 && branchFactor > 0; j--) {
            if (j != n && g.isFeasible(n, j)) {
                actions.add(new Action(n, j));
                branchFactor--;
            }
        }
        return actions;
    }

    private int getBranchFactor(Graph g, int n) {
        if (decreaseBranchFactor) {
            double mindist = g.D[0][1];
            double maxdist = g.D[0][g.getN()];
            double diff = maxdist - mindist;
            double currentDist = g.D[0][n];// - mindist;
            return Math.min(BRANCH_FACTOR, 1+(int)Math.ceil(2*BRANCH_FACTOR * (1 - currentDist / maxdist)));
        }
        else
            return BRANCH_FACTOR;
    }

    /**
     * Get the possible actions by considering FIRST the connections with the leaves, and then with the other
     * nodes considered before
     * @param g
     * @param n
     * @return
     */
    private ArrayList<Action> myGetActions2(Graph g, int n) {
        ArrayList<Action> actions = new ArrayList<>();
        int branchFactor = getBranchFactor(g, n);
        // Check the leaves first

        for (int j = g.getN(); j > 0 && branchFactor > 0; j--) {
            if (j != n && g.revSolution[j].size() == 0 && g.isFeasible(n, j)) {
                actions.add(new Action(n, j));
                branchFactor--;
            }
        }
        actions.sort(Comparator.comparingDouble(a -> g.D[a.getParent()][n]));
//        actions.sort(Comparator.comparingDouble(a -> g.potential[a.getParent()] + g.D[a.getParent()][a.getNode()]));
        // Always connect to school
        actions.add(new Action(n, 0));

        // Check other nodes

        for (int j = g.getN(); j > 0 && branchFactor > 0; j--) {
            if (j != n && g.revSolution[j].size() != 0 && g.isFeasible(n, j)) {
                actions.add(new Action(n, j));
                branchFactor--;
            }
        }

        return actions;
    }

    /**
     * Get the possible actions by considering FIRST the connections with the leaves, and then with the other
     * nodes considered before
     * @param g
     * @param n
     * @return
     */
    private ArrayList<Action> myGetActions3(Graph g, int n) {
        ArrayList<Action> actions = new ArrayList<>();
        int branchFactor = getBranchFactor(g, n);
        // Check the leaves first

        SortedMap<Double, Action> actionMap = new TreeMap<>();
        for (int i = 1; i <= g.getN(); i++) {
            if (g.isFeasible(n, i)) {
                actionMap.put(g.points.get(n).distTo(g.points.get(i)), new Action(n, i));
            }
        }
        for (Action a : actionMap.values()) {
            actions.add(a);
            branchFactor--;
            if (branchFactor <= 0)
                break;
        }
        actions.add(new Action(n, 0));

        return actions;
    }

    public void partialSolve(Graph g, int maxiterations) {

        Graph g2 = g.softCopy();
        g2.completeSolutionGreedy2(1, -1);
        if ((g2.getSolutionLeaves() < nleaves) || (g2.getSolutionLeaves() == nleaves && g2.getSolutionRisk() < risk)) {
            System.out.println("Update: #leaves: " + g2.getSolutionLeaves() + " risk: " + g2.getSolutionRisk());
            nleaves = g2.getSolutionLeaves();
            risk = g2.getSolutionRisk();
            bestSol = g2;
            if (viewer != null)
                viewer.setGraph(bestSol.softCopy());
            lastUpdateTime = System.currentTimeMillis();
        }

        partialSolve(g, 1, maxiterations);
    }

    private void partialSolve(Graph g, int n, int maxiterations) {
        if (n > g.getN()) {
            if ((g.getSolutionLeaves() < nleaves) || (g.getSolutionLeaves() == nleaves && g.getSolutionRisk() < risk)) {
                System.out.println("Update: #leaves: " + g.getSolutionLeaves() + " risk: " + g.getSolutionRisk());
                bestSol = g.softCopy();
                nleaves = g.getSolutionLeaves();
                risk = g.getSolutionRisk();
                if (viewer != null)
                    viewer.setGraph(bestSol.softCopy());
                lastUpdateTime = System.currentTimeMillis();
            }
            return;
        }

        if (n > maxiterations) {
            // Complete the solution with a greedy method
            g.completeSolutionGreedy2(maxiterations, nleaves);
//            if (solverState == SOLVE)
//                g.simulatedAnnealing();
            if ((g.getSolutionLeaves() < nleaves) || (g.getSolutionLeaves() == nleaves && g.getSolutionRisk() < risk)) {
                System.out.println("Update: #leaves: " + g.getSolutionLeaves() + " risk: " + g.getSolutionRisk());
                bestSol = g.softCopy();
                nleaves = g.getSolutionLeaves();
                risk = g.getSolutionRisk();
                if (viewer != null)
                    viewer.setGraph(bestSol.softCopy());
                lastUpdateTime = System.currentTimeMillis();
            }
            return;
        }

        if (Main.executionTimeFinished() || timeoutExpired())
            return;
        if (solverState == SOLVE && Main.solveTimeFinished())
            return;
        if (solverState == PRESOLVE && Main.presolveTimeFinished())
            return;

        ArrayList<Action> actions = myGetActions2(g, n);

        for (int i = 0; i < actions.size(); i++) {
            Graph g2 = g.softCopy();
            g2.updateSolution(actions.get(i));
            if (bound(g2, n)) {
                continue;
            }
            partialSolve(g2,n+1, maxiterations);
            if (Main.executionTimeFinished() || timeoutExpired())
                return;
        }
    }


    /**
     * Check the number of leaves and the risk of the relaxation for g.
     * @param g
     * @param iteration
     * @return true if the relaxation is worse than the optimal solution found so far
     */
    private boolean bound(Graph g, int iteration) {
        int leaves = 0;
        for (int i = 1; i < iteration; i++)
            if (g.revSolution[i].size() == 0)
                leaves++;
        return leaves >= nleaves;
    }

    public void setBranchFactor(int bfactor) {
        this.BRANCH_FACTOR = bfactor;
    }

    public Graph getBestSol() {
        return bestSol;
    }

    public void setViewer(GraphViewer viewer) {
        this.viewer = viewer;
    }
    public int getSolverState() {
        return solverState;
    }

    public void setSolverState(int solverState) {
        this.solverState = solverState;
        lastUpdateTime = System.currentTimeMillis();
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
        lastUpdateTime = System.currentTimeMillis();
    }

    public boolean timeoutExpired() {
        if ((System.currentTimeMillis() - lastUpdateTime)/1000 >= timeout)
            System.out.println("Timeout expired");
        return (System.currentTimeMillis() - lastUpdateTime)/1000 >= timeout;
    }
}
