package pedibus;

import java.util.*;

/**
 * Created by andrea on 25/01/17.
 */
public class Graph {


    private int N;                  // Number of nodes DIFFERENT FROM 0 (so total nodes are N + 1)
    double alpha;
    List<Coord> points;     // Nodes are defined as list of coordinates
    double[][] risk;        // Risk matrix (!! it may not be symmetric)
    double[][] D;           // Distances

    private List<Integer>[] adjList;        // Adjacent list: i --> j   (from node to school)
    private List<Integer>[] revAdjList;     // Reverse list: j --> i    (from school to node)


    private int[] solution;  // Adjacent list of the solution (since it is a tree, you only need to store the parent for each node
    protected List<Integer>[] revSolution;  // Reverse adjacent list of the solution
    double[] potential;     // Potential of a node : sum of distances from node to school


    private Graph() {}

    public Graph(int N, double alpha, int[] coordX, int[] coordY, double[][] risk) {
        this.N = N;
        this.alpha = alpha;
        this.risk = risk;

        // Create arrays and lists
        points = new ArrayList<>(N + 2);
        D = new double[N + 1][N + 1];
        adjList = new ArrayList[N + 1];
        revAdjList = new ArrayList[N + 1];

        for (int i = 0; i <= N; i++) {
            adjList[i] = new ArrayList<>();
            revAdjList[i] = new ArrayList<>();
        }

        solution = new int[N + 1];
        revSolution = new ArrayList[N + 1];
        for (int i = 0; i <= N; i++)
            revSolution[i] = new ArrayList<>();
        for (int i = 1; i <= N; i++)
            revSolution[0].add(i);
        potential = new double[N + 1];


        Coord root = new Coord(coordX[0], coordY[0], 0);

        // Save all bus stops as a list of coordinates
        for (int i = 0; i <= N; i++)
            points.add(new Coord(coordX[i], coordY[i], i));     // !! Keep a reference to original position for answer

        // Order points based on distance from school
        points.sort((o1, o2) -> (int)(o1.distTo(root) - o2.distTo(root)));


        // Compute distance matrix
        for (int i = 0; i <= N; i++) {
            for (int j = i+1; j <= N; j++) {
                D[i][j] = points.get(i).distTo(points.get(j));
                D[j][i] = D[i][j];
            }
        }

        // Set initial potential and solution
        for (int i = 0; i <= N; i++) {
            potential[i] = D[i][0];
            solution[i] = 0;            // All nodes connected to the school
        }

        build_graph();
    }

    /**
     * Make a soft copy of the graph.
     * WARNING: Only effectively copy the solution, revSolution and potential. Other data are only shared.
     * @return
     */
    public Graph softCopy() {
        Graph g = new Graph();
        g.points = points;
        g.adjList = adjList;
        g.revAdjList = revAdjList;
        g.N = N;
        g.risk = risk;
        g.alpha = alpha;
        g.D = D;
        g.solution = new int[N + 1];
        g.revSolution = new ArrayList[N + 1];
        g.potential = new double[N + 1];
        for (int i = 0; i <= N; i++) {
            g.solution[i] = solution[i];
            g.revSolution[i] = new ArrayList<>(revSolution[i]);
            g.potential[i] = potential[i];
        }

        return g;
    }

    private void build_graph() {
        int arcs = 0;
        for (int i = 0; i <= N; i++) {
            for (int j = 0; j <= N; j++) {
                // Connect node i to node j only if it is possible
                if (i != j && D[i][j] + D[j][0] <= alpha*D[i][0]) {
                    adjList[i].add(j);
                    revAdjList[j].add(i);
                    arcs++;
                }
            }
        }
    }


    public void printGraph() {
        for (int i = 0; i <= N; i++) {
            System.out.print("Node " + i + ": ");
            for (int j = 0; j < adjList[i].size(); j++)
                System.out.print(adjList[i].get(j) + " ");
            System.out.print("\n");
        }

        for (int i = 0; i <= N; i++) {
            System.out.print("REV Node " + i + ": ");
            for (int j = 0; j < revAdjList[i].size(); j++)
                System.out.print(revAdjList[i].get(j) + " ");
            System.out.print("\n");
        }
    }

    /**
     * Create a list of leaves and connect all nodes to the first leaf they found
     * @return
     */
    public int[][] greedy() {
        int[][] sol = new int[N+1][2];
        int solCount = 0;
        ArrayList<Integer> leaves = new ArrayList<>();
        double[] dist = new double[N + 1];


        for (int i = 0; i <= N; i++)
            dist[i] = -1;
        leaves.add(0);

        for (int node = 1; node <= N; node++) {
            // Find closest leaf to node
            double closest_dist = 1<<20;

        }


        while (solCount < N) {
            boolean chosen  = false;

            for (int i = leaves.size() - 1; i > 0; i--) {
                int leaf = leaves.get(i);
                for (int adj : adjList[leaf]) {
                    if (dist[adj] == -1 && dist[leaf] + D[leaf][adj] <= alpha*D[adj][0]) {
                        chosen = true;
                        dist[adj] = dist[leaf] + D[leaf][adj];
                        leaves.remove(i);
                        leaves.add(adj);
                        sol[solCount][0] = adj;
                        sol[solCount][1] = leaf;
                        solCount ++;
                        break;
                    }
                }
                if (chosen) break;
            }

            if (!chosen) {
                for (int i = 1; i <= N; i++) {
                    if (dist[i] == -1) {
                        dist[i] = D[i][0];
                        leaves.add(i);
                        sol[solCount][0] = i;
                        sol[solCount][1] = 0;
                        solCount++;
                        break;
                    }
                }
            }
        }

        System.out.println("Solution found: # of leaves: " + (leaves.size() ));
        for (int i = 0; i < N; i++) {
            System.out.println(points.get(sol[i][0]).idx() + " " + points.get(sol[i][1]).idx());
        }

        return sol;
    }


    public boolean hillClimbing() {
        boolean hasImproved = false;
        List<Action> actions;
        int numIterations = 10000;
        // Perform hill climbing
        boolean improve;
        do {
            numIterations--;
            actions = getActions();
//            Collections.shuffle(actions, new Random());
//            Optional<Action> best = actions.stream().filter(x -> x.getValue() < 0).findAny();
            Optional<Action> best = actions.stream().min(Comparator.comparingDouble(Action::getValue));
            if (best.isPresent() && best.get().getValue() < 0) {
                // Do the action
                updateSolution(best.get());
                improve = true;
                hasImproved = true;
            }
            else
                improve = false;

        } while (improve && numIterations > 0);
        return hasImproved;
    }

    public boolean hillClimbing2() {
        boolean hasImproved = false;
        List<Action2> actions;
        int numIterations = 10000;
        // Perform hill climbing
        boolean improve;
        do {
            numIterations--;
            actions = getActions2();
            Optional<Action2> best = actions.stream().min(Comparator.comparingDouble(Action2::getValue));
            if (best.isPresent() && best.get().getValue() < 0) {
                // Do the action
                updateSolution2(best.get());
                improve = true;
                hasImproved = true;
            }
            else
                improve = false;

        } while (improve && numIterations > 0);
        return hasImproved;
    }

    private double getTemperature(int iteration) {
        double q = 0.85;
        int L = 5;
        double T0 = 100;
        return T0 * Math.pow(q, (double)(iteration / L));
    }

    public boolean simulatedAnnealing() {
        boolean foundBetterSolution = false;
        boolean end = false;

        Graph bestSol = softCopy();

        int nleaves = bestSol.getSolutionLeaves();
        double risk = bestSol.getSolutionRisk();

        for (int iteration = 1; iteration <= 100 && !end; iteration++) {
            double T = getTemperature(iteration);
            Graph g = bestSol.softCopy();
            for (int i = 0; i < (int)T+1; i++) {
                List<Action2> actions = g.getActions2();
                Collections.shuffle(actions);
                if (actions.size() > 0) {
                    g.updateSolution2(actions.get(0));
                }
                else
                    end = true;
            }
            g.hillClimbingIterated();

            if (g.getSolutionLeaves() < nleaves || (g.getSolutionLeaves() == nleaves && g.getSolutionRisk() < risk)) {
                bestSol = g.softCopy();
                nleaves = bestSol.getSolutionLeaves();
                risk = bestSol.getSolutionRisk();
                foundBetterSolution = true;
            }
            if (T <= 0)
                break;
            if (Main.executionTimeFinished())
                break;
        }
        if (foundBetterSolution) {
            solution = bestSol.solution;
            revSolution = bestSol.revSolution;
            potential = bestSol.potential;
        }
        return foundBetterSolution;
    }

    /**
     * Always try to connect a node to the closest one he find
     */
    public void closestNodeGreedy() {
        List<Integer> leaves = new ArrayList<>();
        for (int i = 1; i <= N; i++) {
            // Find closest leaf
            double mindist = Double.MAX_VALUE;
            int minnode = 0;
            for (int j = 0; j < leaves.size(); j++) {
                if (isFeasible(i, leaves.get(j))) {
                    if (D[i][leaves.get(j)] < mindist) {
                        mindist = D[i][leaves.get(j)];
                        minnode = leaves.get(j);
                    }
                }
            }
            // Connect to minnode
            updateSolution(new Action(i, minnode));
            if (minnode != 0) {
                leaves.remove(leaves.indexOf(minnode));
            }
            leaves.add(i);
        }
    }

    public void closestNodeGreedy2() {
        List<Integer> leaves = new ArrayList<>();

        boolean[] visited = new boolean[N + 1];
        visited[0] = true;


        for (int i = 1; i <= N; i++) {
            // Find closest leaf
            double mindist = Double.MAX_VALUE;
            Action bestAction = null;

            for (int j = 0; j < leaves.size(); j++) {
                for (int k = 1; k <= N; k++) {
                    if (!visited[k]) {
                        // Try to connect leaves.get(j) to k
                        if (isFeasible(k, leaves.get(j))) {
                            if (D[k][leaves.get(j)] < mindist) {
                                mindist = D[k][leaves.get(j)];
                                bestAction = new Action(k, leaves.get(j));
                            }
                        }
                    }
                }
            }
            if (bestAction == null) {
                // Connect closest node to 0
                for (int k = 1; k <= N; k++)
                    if (!visited[k]) {
                        bestAction = new Action(k, 0);
                        break;
                    }
            }
            // Connect to minnode
            updateSolution(bestAction);
            visited[bestAction.getNode()] = true;

            if (bestAction.getParent() != 0)
                leaves.remove(leaves.indexOf(bestAction.getParent()));
            leaves.add(bestAction.getNode());
        }
    }


    public void completeSolutionGreedy(int startingNode, int maxLeaves) {
        List<Integer> leaves = new ArrayList<>();

        boolean[] visited = new boolean[N + 1];
        for (int i = 0; i < startingNode; i++)
            visited[i] = true;

        for (int i = 1; i < startingNode; i++)
            if (revSolution[i].size() == 0)
                leaves.add(i);


        for (int i = startingNode; i <= N; i++) {
            // Find closest leaf
            double mindist = Double.MAX_VALUE;
            Action bestAction = null;

            for (int j = 0; j < leaves.size(); j++) {
                for (int k = startingNode; k <= N; k++) {
                    if (!visited[k]) {
                        // Try to connect leaves.get(j) to k
                        if (isFeasible(k, leaves.get(j))) {
                            if (D[k][leaves.get(j)] < mindist) {
                                mindist = D[k][leaves.get(j)];
                                bestAction = new Action(k, leaves.get(j));
                            }
                        }
                    }
                }
            }
            if (bestAction == null) {
                // Connect closest node to 0
                for (int k = 1; k <= N; k++)
                    if (!visited[k]) {
                        bestAction = new Action(k, 0);
                        break;
                    }
            }
            // Connect to minnode
            updateSolution(bestAction);
            visited[bestAction.getNode()] = true;

            if (bestAction.getParent() != 0)
                leaves.remove(leaves.indexOf(bestAction.getParent()));
            leaves.add(bestAction.getNode());

            if (maxLeaves > 0 && leaves.size() > maxLeaves)
                break;
        }
    }

    public void swapNodes(int node) {
        int father = solution[node];
        int grandfather = solution[father];
        int grandgrandfather = solution[grandfather];

        if (grandfather == 0)
            return;

        // Now the situation is node -> father -> grandfather -> grandgrandfather
        // We want to swap nodes to be like: node -> grandfather -> father -> grandgrandfather
        Graph copy = softCopy();

        // Connect all nodes to the school
        copy.updateSolution(new Action(node, grandfather));
        copy.updateSolution(new Action(father, grandgrandfather));
        copy.updateSolution(new Action(grandfather, father));

        if (copy.checkPathFeasible(node) && copy.potential[node] <= potential[node]) {
            updateSolution(new Action(node, grandfather));
            updateSolution(new Action(father, grandgrandfather));
            updateSolution(new Action(grandfather, father));
        }
//        swapNodes(solution[node]);
    }

    /**
     * Check the feasibility of all the arcs starting from node down to the school
     * @param node a node in the graph
     * @return true if all the path from node to the school is feasible
     */
    public boolean checkPathFeasible(int node) {
        if (node == 0)
            return true;
        return (potential[node] <= alpha * D[node][0]) && checkPathFeasible(solution[node]);
    }

    public void updateSolution(Action action) {
        int source = action.getNode();
        int dest = action.getParent();
        int prevDest = solution[source];

        // Add the arc source --> dest
        solution[source] = dest;
        potential[source] = potential[dest] + D[source][dest];
        revSolution[prevDest].remove(revSolution[prevDest].indexOf(source));
        revSolution[dest].add(source);
        Queue<Integer> Q = new LinkedList<>();
        Q.add(source);
        while (!Q.isEmpty()) {
            int u = Q.poll();
            potential[u] = potential[solution[u]] + D[u][solution[u]];
            for (int v : revSolution[u])
                Q.add(v);
        }
    }

    public int getSolutionLeaves() {
        int leaves = 0;
        for (int i = 1; i <= N; i++)
            if (revSolution[i].size() == 0)
                leaves++;
        return leaves;
    }

    public double getSolutionRisk() {
        double solRisk = 0.0;
        for (int i = 1; i <= N; i++) {
            solRisk += risk[points.get(i).idx()][points.get(solution[i]).idx()];
        }

        return Math.round(solRisk*10000d)/10000d;
    }


    /**
     * Check if source --> dest is feasible or not. It is feasible if it satisfies the distance constraint
     * @param source
     * @param dest
     * @return
     */
    public boolean isFeasible(int source, int dest) {

        boolean feasible = false;
        if (D[source][dest] + potential[dest] <= alpha * D[source][0]) {        // Valid for this node
            // Check all the nodes connected to source
            double newPot = D[source][dest] + potential[dest];
//            double diffPot = potential[source] - newPot;        // This is the added distance induced by the action
            double diffPot = newPot - potential[source];
            if (diffPot <= 0)
                feasible = true;        // If the length decreases it is always feasible
            else {
                // Performs a bfs on reversed arcs
                Queue<Integer> Q = new LinkedList<>();
                Q.add(source);
                feasible = true;
                while (!Q.isEmpty() && feasible) {
                    int u = Q.poll();
                    if (potential[u] + diffPot > alpha * D[u][0])
                        feasible = false;

                    for (int v : revSolution[u])    // Add all nodes connected to u
                        Q.add(v);
                }
            }
        }
        if (feasible)
            feasible = remainsAcycle(source, dest);
        return feasible;
    }

    public boolean remainsAcycle(int source, int dest) {
        boolean[] visited = new boolean[N + 1];
        int node = dest;
        while (!visited[source] && node != 0) {
            visited[node] = true;
            node = solution[node];
        }

        return !visited[source];
    }

    public double getImprovement(Action action) {
        int source = action.getNode();
        int dest = action.getParent();
        int destPrev = solution[source];        // Previous connection

        int currentLeaves = 0;
        int afterLeaves = 0;

        for (int i = 1; i <= N; i++) {
            int count = revSolution[i].size();
            if (count == 0)
                currentLeaves++;
            if (i == dest)
                count++;
            if (i == destPrev)
                count--;

            if (count == 0)
                afterLeaves++;
        }
        double value;
        if (revSolution[dest].size() == 0 && revSolution[destPrev].size() > 1)
            value = -100;
        else if (revSolution[destPrev].size() == 1 && revSolution[dest].size() != 0)
            value = 100;
        else
            value = 0;

//        double value = 100 * (afterLeaves - currentLeaves);

        value = value + risk[points.get(source).idx()][points.get(dest).idx()] - risk[points.get(source).idx()][points.get(destPrev).idx()];

        return Math.round(value * 100000d)/100000d;         // Round the number to a precision of 5 digits
    }

    public double getImprovement2(Action2 action) {
        int node = action.getNode();
        int path = action.getPath();

        int node_idx = points.get(node).idx();
        int solnode_idx = points.get(solution[node]).idx();
        int path_idx = points.get(path).idx();
        int solpath_idx = points.get(solution[path]).idx();

        double value = 0.0;

        if (revSolution[node].size() == 0 && revSolution[solution[node]].size() > 1)
            value = -1000.0;
        value -= risk[node_idx][solnode_idx];
        for (int u : revSolution[node]) {
            int u_idx = points.get(u).idx();
            value = value + risk[u_idx][solnode_idx] - risk[u_idx][node_idx];
        }
        value = value - risk[path_idx][solpath_idx] + risk[path_idx][node_idx] + risk[node_idx][solpath_idx];

        return Math.round(value * 100000d)/100000d;         // Round the number to a precision of 5 digits
    }

    /**
     * Return a list of all feasible actions
     * @return
     */
    public List<Action> getActions() {
        List<Action> actions = new ArrayList<>();
        for (int i = 1; i <= N; i++) {
            for (int j = 0; j < adjList[i].size(); j++) {
                if (isFeasible(i, j)) {
                    Action move = new Action(i, j);
                    move.setValue(getImprovement(move));
                    actions.add(move);
                }
            }
        }
        return actions;
    }



    // Getters and setters



    public int getN() { return N; }
    public double getAlpha() { return alpha; }
    public List<Coord> getPoints() { return points; }
    public double[][] getRisk() { return risk; }
    public double[][] getD() { return D; }
    public List<Integer>[] getAdjList() { return adjList; }
    public List<Integer>[] getRevAdjList() { return revAdjList; }
    public double getPot(int node) {return potential[node]; }
    public int[] getSolution() { return solution; }

    /**
     * Remove arc from the adj list (from node to school)
     * @param source
     * @param dest
     */
    public void removeArc(int source, int dest) {
        int idx = adjList[source].indexOf(dest);
        int revidx = revAdjList[dest].indexOf(source);
        if (idx != -1) {
            adjList[source].remove(idx);
            revAdjList[dest].remove(revidx);
        }
    }

    /**
     * Remove the arc (j-->i) (from school to node)
     * @param source
     * @param dest
     */
    public void removeRevArc(int source, int dest) {
        int idx = revAdjList[source].indexOf(dest);
        int revidx = adjList[dest].indexOf(source);
        if (idx != -1) {
            revAdjList[source].remove(idx);
            adjList[dest].remove(revidx);
        }
    }

    public void completeSolutionGreedy2(int startingNode, int maxLeaves) {
        List<Integer> leaves = new ArrayList<>();

        boolean[] visited = new boolean[N + 1];
        for (int i = 0; i < startingNode; i++)
            visited[i] = true;

        for (int i = 1; i < startingNode; i++)
            if (revSolution[i].size() == 0)
                leaves.add(i);


        for (int i = startingNode; i <= N; i++) {
            int mincount = Integer.MAX_VALUE;
            int parent = 0;

            for (int j = 0; j < leaves.size(); j++) {
                int count = 0;
                for (int k = startingNode; k <= N; k++) {
                    if (!visited[k] && isFeasible(k, leaves.get(j)))
                        count++;
                }
                if (count > 0 && count < mincount) {
                    mincount = count;
                    parent = leaves.get(j);
                }
            }

            double mindist = Double.MAX_VALUE;
            int node = startingNode;
            for (int k = startingNode; k <= N; k++) {
                if (k != parent && !visited[k] && isFeasible(k, parent)) {
                    if (D[k][parent] < mindist) {
                        mindist = D[k][parent];
                        node = k;
                    }
                }
            }
            

            updateSolution(new Action(node, parent));
            visited[node] = true;
            if (parent != 0) {
                leaves.remove(leaves.indexOf(parent));
            }
            leaves.add(node);

            swapNodes(node);

            if (maxLeaves > 0 && leaves.size() > maxLeaves)
                return;
        }
        eliminatePathIterated();
        hillClimbingIterated();
    }

    public void updateSolution2(Action2 action) {
        int node = action.getNode();
        int path = action.getPath();

        List<Integer> to_modify = new ArrayList<>(revSolution[node]);
        for (int u : to_modify)
            updateSolution(new Action(u, solution[node]));
        updateSolution(new Action(node, solution[path]));
        updateSolution(new Action(path, node));
    }

    public List<Action2> getActions2() {
        List<Action2> actions = new ArrayList<>();
        for (int node = 1; node <= N; node++) {
            for (int path = 1; path <= N; path++) {
                if (node != path && node != solution[path] && isFeasible2(node, path)) {
                    Action2 action2 = new Action2(node, path);
                    action2.setValue(getImprovement2(action2));
                    actions.add(action2);
                }
            }
        }
        return actions;
    }

    public boolean isFeasible2(int node, int path) {
        if (potential[solution[path]] + D[solution[path]][node] > alpha * D[0][node])
            return false;

        double diff = D[path][node] + D[node][solution[path]] - D[path][solution[path]];
        Queue<Integer> Q = new LinkedList<>();
        Q.add(path);

        boolean feasible = true;
        while (!Q.isEmpty() && feasible) {
            int n = Q.poll();
            if (potential[n] + diff > alpha * D[n][0])
                feasible = false;

            for (int u : revSolution[n])
                Q.add(u);
        }

        return feasible;
    }

    private double pointToLineDistance(int node, int path) {
        Coord p = points.get(node);
        Coord a = points.get(path);
        Coord b = points.get(solution[path]);
        return Math.abs((b.Y() - a.Y())*p.X() - (b.X()-a.X())*p.Y() + (b.X()*a.Y()) - (b.Y()*a.X())) / Math.sqrt(Math.pow(b.Y() - a.Y(), 2) + Math.pow(b.X()-a.X(), 2));
    }

    public boolean eliminatePath(int leaf) {
        List<Integer> nodesToDelete = new ArrayList<>();
        int n = leaf;
        while (n != 0 && revSolution[n].size() <= 1) {
            nodesToDelete.add(n);
            n = solution[n];
        }

        Graph g = softCopy();

        // Connect all the nodes in the path to the school
        for (int node : nodesToDelete)
            g.updateSolution(new Action(node, 0));

        Collections.sort(nodesToDelete);
        Queue<Integer> Q = new LinkedList<>(nodesToDelete);
        boolean feasible = true;
        while (!Q.isEmpty() && feasible) {
            // Eliminate node
            int node = Q.poll();
            List<Action2> actions = g.getActions2();
//            actions.sort(Comparator.comparingDouble(a -> g.pointToLineDistance(a.getNode(), a.getPath())));

            Optional<Action2> act = actions.stream().filter(a -> a.getNode() == node && !Q.contains(a.getPath())).findFirst();
            List<Action> tmp = new ArrayList<>();
            for (int i = 1; i <= g.getN(); i++) {
                if (g.revSolution[i].size() == 0 && !Q.contains(i) && g.isFeasible(node, i))
                    tmp.add(new Action(node, i));
            }
            tmp.sort(Comparator.comparingDouble(a -> D[a.getParent()][node]));


            if (act.isPresent()) {
                g.updateSolution2(act.get());
            }
            else if (tmp.size() > 0) {
                g.updateSolution(tmp.get(0));
            }
            else
                feasible = false;
        }

        if (feasible) {
            solution = g.solution;
            potential = g.potential;
            revSolution = g.revSolution;
        }

        return feasible;
    }

    public void eliminatePathIterated() {
        boolean improve = true;
        while (improve) {
            improve = false;
            for (int i = 1; i <= N; i++)
                if (revSolution[i].size() == 0)
                    improve = improve || eliminatePath(i);
        }
    }

    public void hillClimbingIterated() {
        boolean improved;
        do {
            hillClimbing();
//            System.out.println("hillClimbing: #leaves = " + getSolutionLeaves() + " risk = " + getSolutionRisk());
            if (Main.executionTimeFinished())
                break;
            improved = hillClimbing2();
//            System.out.println("hillClimbing2: #leaves = " + getSolutionLeaves() + " risk = " + getSolutionRisk());
        }
        while (improved);
    }

    public int getSureLeafCount() {
        int count = 0;
        for (int i = 1; i <= N; i++) {
            if (adjList[i].size() == 1)
                count++;
        }
        return count;
    }
}
