package pedibus.gui;

import pedibus.Coord;
import pedibus.Graph;
import pedibus.Action;

import javax.swing.*;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


/**
 * Created by andrea on 14/02/17.
 */
public class GraphViewer extends JFrame {
    private static final int CROSS_DIM = 5;
    private static final int MARGIN_TOP = 40;
    private static final int MARGIN_SIDE = 20;
    private static final int MARGIN_BOTTOM = 20;
    private static final int CIRCLE_RADIUS = 4;

    private Graph graph;
    private List<Coord> graphPoints = new ArrayList<>();
    private int nodeFocus = -1;
    private Coord dragPoint = null;


    public GraphViewer(Graph graph) {
        super();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(640, 640);
        this.graph = graph;

        this.setResizable(true);

        MyListener listener = new MyListener();
        this.addMouseListener(listener);
        this.addMouseMotionListener(listener);
    }

    public void setGraph(Graph g) {
        this.graph = g;
        this.repaint();
    }

    private void paintGraph(Graphics2D g2) {
        int w = getWidth()-2*MARGIN_SIDE;
        int h = getHeight()-MARGIN_TOP - MARGIN_BOTTOM;

        double minY = graph.getPoints().stream().map(Coord::Y).min(Comparator.naturalOrder()).get();
        double maxY = graph.getPoints().stream().map(Coord::Y).max(Comparator.naturalOrder()).get();
        double minX = graph.getPoints().stream().map(Coord::X).min(Comparator.naturalOrder()).get();
        double maxX = graph.getPoints().stream().map(Coord::X).max(Comparator.naturalOrder()).get();

        double gWidth = maxX - minX;
        double gHeight = maxY - minY;

        double scaleFactor = Math.min((double)w/gWidth, (double)h/gHeight);

        double PADDING_SIDE = (w - gWidth*scaleFactor)/2.0;
        double PADDING_TOP = (h - gHeight*scaleFactor)/2.0;

        List<Coord> points = graph.getPoints();

        // Rebuild graph points
        graphPoints.clear();
        for (int i = 0; i < points.size(); i++) {
            int x1 = MARGIN_SIDE + (int)(PADDING_SIDE + (points.get(i).X() - minX) * scaleFactor);
            int y1 = MARGIN_TOP + (int)PADDING_TOP + (int)((gHeight - points.get(i).Y() + minY) * scaleFactor);

            Coord c = new Coord(x1, y1, points.get(i).idx());
            graphPoints.add(c);
        }

        // Draw the links
        for (int i = 1; i < graphPoints.size(); i++) {
            line(g2, graphPoints.get(i).X(), graphPoints.get(i).Y(), graphPoints.get(graph.getSolution()[i]).X(), graphPoints.get(graph.getSolution()[i]).Y());
        }

        // Draw the nodes
        for (int i = 1; i < graphPoints.size(); i++) {
            circle(g2, graphPoints.get(i).X(), graphPoints.get(i).Y());
        }

        // Draw the school
        g2.setColor(Color.BLUE);
        cross(g2, (int)graphPoints.get(0).X(), (int)graphPoints.get(0).Y());
        g2.setColor(Color.BLACK);

        if (nodeFocus != -1) {
            g2.setColor(Color.RED);
            Stroke prevStroke = g2.getStroke();
            g2.setStroke(new BasicStroke(3));
            cross(g2, graphPoints.get(nodeFocus).X(), graphPoints.get(nodeFocus).Y());
            g2.setStroke(prevStroke);

            for (int i = 0; i < graphPoints.size(); i++) {
                if (i != nodeFocus && graph.isFeasible(nodeFocus, i)) {
                    line(g2, graphPoints.get(nodeFocus), graphPoints.get(i));
                }
            }

            g2.setColor(Color.BLACK);
        }

        if (dragPoint != null && nodeFocus != -1) {
            line(g2, graphPoints.get(nodeFocus), dragPoint);
        }

    }

    private void line(Graphics2D g2, double x, double y, double x1, double y1) {
        g2.drawLine((int)x, (int)y, (int)x1, (int)y1);
    }

    private void line(Graphics2D g2, Coord c1, Coord c2) {
        line(g2, c1.X(), c1.Y(), c2.X(), c2.Y());
    }

    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.clearRect(0, 0, this.getWidth(), this.getHeight());

        paintGraph(g2);
    }

    private void cross(Graphics2D g2, int x, int y) {
        g2.drawLine(x - CROSS_DIM, y - CROSS_DIM, x + CROSS_DIM, y + CROSS_DIM);
        g2.drawLine(x-CROSS_DIM, y + CROSS_DIM, x + CROSS_DIM, y - CROSS_DIM);
    }

    private void cross(Graphics2D g2, double x, double y) {
        cross(g2, (int)x, (int)y);
    }
    
    private void circle(Graphics2D g2, int x, int y) {
        g2.drawOval(x - CIRCLE_RADIUS, y - CIRCLE_RADIUS, 2*CIRCLE_RADIUS, 2*CIRCLE_RADIUS);
    }

    private void circle(Graphics2D g2, double x, double y) {
        circle(g2, (int)x, (int)y);
    }


    class MyListener implements MouseListener, MouseMotionListener {

        private static final double MIN_DIST = 7;

        @Override
        public void mouseClicked(MouseEvent e) {}

        @Override
        public void mousePressed(MouseEvent e) {
            nodeFocus = getClosestNode(e.getX(), e.getY());
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            dragPoint = null;
            int node = getClosestNode(e.getX(), e.getY());
            if (nodeFocus != -1 && node != -1 && graph.isFeasible(nodeFocus, node)) {
                graph.updateSolution(new Action(nodeFocus, node));
            }
            repaint();
        }

        @Override
        public void mouseEntered(MouseEvent e) {}

        @Override
        public void mouseExited(MouseEvent e) {}

        @Override
        public void mouseDragged(MouseEvent e) {
            if (nodeFocus != -1) {
                if (dragPoint != null) {
                    dragPoint.setX(e.getX());
                    dragPoint.setY(e.getY());
                    repaint();
                } else
                    dragPoint = new Coord(e.getX(), e.getY(), -1);
            }
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            if (dragPoint == null) {
                int node = getClosestNode(e.getX(), e.getY());
                if (node != nodeFocus) {
                    nodeFocus = node;
                    repaint();
                }
            }
        }

        private int getClosestNode(int x, int y) {
            double dist = Double.MAX_VALUE;
            int node = -1;
            Coord current = new Coord(x, y, -1);
            for (int i = 0; i < graphPoints.size(); i++) {
                double d = graphPoints.get(i).distTo(current);
                if (d < MIN_DIST && d < dist) {
                    dist = d;
                    node = i;
                }
            }
            return node;
        }
    }
}
