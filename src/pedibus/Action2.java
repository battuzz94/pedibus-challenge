package pedibus;

/**
 * This action is defined by a couple of integer < u, v> and the meaning is to place node u inside the path from v
 * to its parent
 */
public class Action2 {
    private int node;
    private int parent;
    private double value;

    public Action2(int node, int path) {
        this.node = node;
        this.parent = path;
    }
    public void setValue(double value) {
        this.value = value;
    }

    public double getValue() {
        return this.value;
    }
    public int getNode() {
        return this.node;
    }
    public int getPath() {
        return this.parent;
    }
}
